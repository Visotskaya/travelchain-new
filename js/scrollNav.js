$('.top_line nav a').click(function () {
   var element = $(this).attr('href');
   var dist = $(element).offset().top;

   $('html, body').animate({'scrollTop': dist}, 1000);

   return false;
});

$(window).scroll(function (){

    $('section[id]').each(function () {
        var id = $(this).attr('id');
        if($(this).offset().top-100 <$(window).scrollTop()){
            $('.top_line nav a[href=#'+id+']').addClass('active').siblings().removeClass('active');
        }

        if($(window).scrollTop() < 900){
            $('.top_line nav a').removeClass('active');
        }
    });

    if($(window).scrollTop() > $('.top_line').offset().top){
        $('.top_line_wrap').addClass('fixed');
    }
    else{
        $('.top_line_wrap').removeClass('fixed');
    }
});
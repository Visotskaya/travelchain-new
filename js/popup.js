$('.open_popup').click(function () {
    if (!$(this).hasClass('open')){
        $(this).addClass('open');
        $('.popup').addClass('active');
        $('.bg_popup').fadeIn();
        $('body').addClass('p_opened');
    }
    else{
        $(this).removeClass('open');
        $('.popup').removeClass('active');
        $('.bg_popup').fadeOut();
        $('body').removeClass('p_opened');
    }

        $('.bg_popup').click(function(){
            $('.popup').removeClass('active');
            $('.bg_popup').fadeOut();
            $('body').removeClass('p_opened');
            $('.open_popup').removeClass('open');
        });

});

$(function () {

    $('.select_style').jselector();

    $('.block_5').each(function () {
        var $pag = $('.slides_menu li', this);
        var $items = $('.slides ul li', this);
        var $prev_arrow = $('.prev', this);
        var $next_arrow = $('.next', this);

        $pag.click(function () {
            var index = $(this).index();
            $items.eq(index).addClass('active').siblings().removeClass('active2').removeClass('active');
            setTimeout(function () {
                $items.eq(index).addClass('active2');
            },100);
            $(this).addClass('active').siblings().removeClass('active');
        }).first().click();

        $prev_arrow.click(function () {
            var index = $pag.filter('.active').index();
            if(index == 0){
                index = $pag.length
            }
            $pag.eq(index - 1).click();
        })

        $next_arrow.click(function () {
            var index = $pag.filter('.active').index();
            if(index == $pag.length - 1){
                index = -1
            }
            $pag.eq(index + 1).click();
        })
    });

    $('.block_9').each(function(){
        var $checker1 = $('.checker1', this);
        var $checker2 = $('.checker2', this);
        var $check = $('.check', this);
        var $content1 = $('.content1', this);
        var $content2 = $('.content2', this);

        $checker1.click(function(){
            if($(this).hasClass('active')){

            }
            else{
                $(this).addClass('active');
                $check.removeClass('active');
                $checker2.removeClass('active');
                $content1.fadeIn();
                $content2.hide();
            }
        });

        $checker2.click(function(){
            if($(this).hasClass('active')){

            }
            else{
                $(this).addClass('active');
                $checker1.removeClass('active');
                $check.addClass('active');
                $content1.hide();
                $content2.fadeIn();
            }
        });

        $check.click(function(){
            if($(this).hasClass('active')){
                $checker2.removeClass('active');
                $(this).removeClass('active');
                $checker1.addClass('active');
                $content1.fadeIn();
                $content2.hide();
            }
            else{
                $checker2.addClass('active');
                $(this).addClass('active');
                $checker1.removeClass('active');
                $content1.hide();
                $content2.fadeIn();
            }
        });

        $('.slide_hide', this).click(function(){
            if($(this).hasClass('active')){
                $(this).removeClass('active').parents('span').siblings('.hide').slideUp();
            }
            else{
                $(this).addClass('active').parents('span').siblings('.hide').slideDown();
            }
            return false;
        });
    });

    $('.block_12').each(function(){
        var $circle1 = $('.circle1', this);
        var $circle2 = $('.circle2', this);
        var $numbers1 = $('.num1', this);
        var $numbers2 = $('.num2', this);
        var $checker1 = $('.checker1', this);
        var $checker2 = $('.checker2', this);
        var $check = $('.check', this);

        $checker1.click(function(){
            if($(this).hasClass('active')){

            }
            else{
                $(this).addClass('active');
                $check.removeClass('active');
                $checker2.removeClass('active');
                $circle1.fadeIn();
                $circle2.hide();
                $numbers1.fadeIn();
                $numbers2.hide();
            }
        });

        $checker2.click(function(){
            if($(this).hasClass('active')){
                
            }
            else{
                $(this).addClass('active');
                $checker1.removeClass('active');
                $check.addClass('active');
                $circle1.hide();
                $circle2.fadeIn();
                $numbers1.hide();
                $numbers2.fadeIn();
            }
        });

        $check.click(function(){
            if($(this).hasClass('active')){
                $checker2.removeClass('active');
                $(this).removeClass('active');
                $checker1.addClass('active');
                $circle1.fadeIn();
                $circle2.hide();
                $numbers1.fadeIn();
                $numbers2.hide();
            }
            else{
                $checker2.addClass('active');
                $(this).addClass('active');
                $checker1.removeClass('active');
                $circle1.hide();
                $circle2.fadeIn();
                $numbers1.hide();
                $numbers2.fadeIn();
            }
        });
    });

    $('.carousel').each(function(){
        var $item = $('.slide ul', this);
        var $prev = $('.prev', this);
        var $next = $('.next', this);
        var $current = $('.count span', this);
        var $count = $('.count ins', this);

        $count.text($item.length);
        $current.text($item.filter('.active').index()+1);

        // $item.addClass('active');

        $prev.click(function(){
            var index = $item.filter('.active').index();
            index--
            if(index == 0){
                $prev.addClass('disabled').attr('disabled','disabled');
            }
            if(index < $item.length){
                $next.removeClass('disabled').removeAttr('disabled');
            }
            $item.eq(index).addClass('active').siblings().removeClass('active');
            setTimeout(function(){
                $item.eq(index).addClass('active2').siblings().removeClass('active2');
            }, 100);
            $current.text(index+1);
        });

        $next.click(function(){
            var index = $item.filter('.active').index();
            index++
            console.log(index);
            if(index == $item.length-1){
                $next.addClass('disabled').attr('disabled','disabled');
            }
            if(index > 0){
                $prev.removeClass('disabled').removeAttr('disabled');
            }
            $item.eq(index).addClass('active').siblings().removeClass('active');
            setTimeout(function(){
                $item.eq(index).addClass('active2').siblings().removeClass('active2');
            }, 100);
            $current.text(index+1);
        });
    });

    $('.acardion').each(function(){
        var $item = $('>li', this);

        $item.click(function(){
            if($(this).hasClass('active')){
                $(this).removeClass('active');
                $(this).find('.block-text').slideUp();
            }
            else{
                $(this).addClass('active');
                $(this).find('.block-text').slideDown();
            }
        });
    });

    $(window).scroll(function(){

        $('.animate').each(function(){
            if($(window).scrollTop() >= $(this).offset().top){
                $(this).addClass('active');
            }
        });
        if ($(window).scrollTop()>$(window).height()){
            $('.social_right_line').addClass('scroll');
        } else{
            $('.social_right_line').removeClass('scroll');
        }
        if ($(window).scrollTop()>50){
            $('.menu.open_popup').addClass('scroll');
        } else{
            $('.menu.open_popup').removeClass('scroll');
        }


        // $('section').each(function(){
        //     if($(this).offset().top <= $(window).scrollTop()+$(window).height()){
        //         var scroll = Number($(window).scrollTop()) + Number($(window).height()) - Number($(this).offset().top);
        //         console.log(scroll)
        //         $(this).find('.scroll_block').css({'top': -scroll});
        //     }
        //     else{
        //         $(this).removeClass('active');
        //     }
        // });

    }).scroll();

    $('.menu_right, .menu_center, .menu, .video').addClass('active');

    setTimeout(function(){
        $('.grayback').css({'left': $('.grayback').data('scroll')});
    }, 1000);

    /**/
    $('body').on('click', '.href-dropdown', function () {
        if (!$(this).parent().hasClass('open')) {
            $('body').find('.menu-dropdown').hide().parent().removeClass('open');
            $(this).next('.menu-dropdown').fadeToggle();
            $(this).parent().addClass('open');
        } else {
            $(this).parent().removeClass('open');
            $(this).next('.menu-dropdown').hide();
        }
    });
  $('.popup ul li a ,.menu_center a').on('click', function () {
    $('body').removeClass('p_opened');
    $('.popup').removeClass('active');
    $('.open_popup').removeClass('open');
    $('.bg_popup').css('display','none');
  });
    $('body').on('click', '.menu-dropdown a', function () {
        var parent = $(this).parentsUntil('.box-dropdown').parent('.box-dropdown');
        if (!$(this).hasClass('active')) {
            parent.find('.change-value').html($(this).html());
            parent.find('.menu-dropdown').find('a').removeClass('active');
            $(this).addClass('active');
        }
        parent.removeClass('open').find('.menu-dropdown').hide();
    });
    $(document).click(function (event) {
        if ($(event.target).closest(".menu-dropdown").length || $(event.target).closest(".href-dropdown").length)
            return;
        $(".menu-dropdown").css('display', 'none');
        if ($(".menu-dropdown").parent().hasClass('open')) {
            $(".menu-dropdown").parent().removeClass('open');
        }
    });
    $('.social_right_line .wrap').midnight();

    //email form
    $( 'form' ).bind('keypress', function(e){
        if(e.keyCode == 13){
            e.preventDefault();
            $(this).find('.send-click').click();
        }
    });
    window.language = $('body').data('lang') || 'ru';
    $('.send-click').click(function (e)
    {
        e.preventDefault();

        var val = $(this).siblings("input[name='email']").val();
        if(val)
        {
            var fd = new FormData();
            fd.append('email', val);
            fd.append('lang', window.language);
            subscribeEmail(fd);
        }
    });
    var subscribeEmail = function (fd) {
        $.ajax({
            url: 'email.php',
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                console.log(data);
                data = JSON.parse(data);
                if(data && data.status=='subscribed'){
                    switch( window.language.toLowerCase() )
                    {
                        case "jp":
                            alert('You have successfully subscribed.');
                            break;

                        case "kr":
                            alert('You have successfully subscribed.');
                            break;

                        case "ru":
                            alert('Вы успешно подписались.');
                            break;

                        default:
                            alert('You have successfully subscribed.');
                    }

                }
                else
                {
                    switch( window.language.toLowerCase() )
                    {
                        case "jp":
                            alert('You wrote your email with an error or you are already subscribed to our news.');
                            break;

                        case "kr":
                            alert('You wrote your email with an error or you are already subscribed to our news.');
                            break;

                        case "ru":
                            alert('Вы написали ваш email с ошибкой или вы уже подписаны на наши новости.');
                            break;

                        default:
                            alert('You wrote your email with an error or you are already subscribed to our news.');
                    }
                }
            },
            error: function() {
                alert('You wrote your email with an error or you are already subscribed to our news.');
            }
        });
    };

});
<?php

$data = [
    'email'     => $_POST['email'],
    'status'    => 'subscribed',
];

switch( strtolower(@$_POST['lang']) )
{
    case "jp":
        $listId =  'a3e80b24a4';
        break;
    case "kr":
        $listId =  '186389fc4c';
        break;
    case "ru":
        $listId =  '61fef09904';
        break;
    case "zh":
        $listId =  '29bcf929cf';
        break;
    case "ar":
        $listId =  '0265414be0';
        break;
    case "es":
        $listId =  'de747647fa';
        break;
    case "de":
        $listId =  'de754cb0be';
        break;
    default: $listId = '3ca04c0a02';
}

$data['listId'] = $listId;
syncMailchimp($data);
function syncMailchimp($data) {
    $apiKey = 'b514651049959c9282eeab81fd825e41-us16';
    $listId = $data['listId'];

    $memberId = md5(strtolower($data['email']));
    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
    $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;
    $json = json_encode([
        'email_address' => $data['email'],
        'status'        => $data['status'], // "subscribed","unsubscribed","cleaned","pending"
    ]);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    $result = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    echo($result);
}


?>